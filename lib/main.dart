import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}


String englishGreeting="Hello Flutter";
String spanishGreeting="Hola Flutter";
String JapanGreeting = "こんにちは Flutter";
String ThaiGreetig = "สวัสดี Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp>{
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context)
  {
        return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
      appBar: AppBar(
      title: Text("Hello Flutter"),
      leading: Icon(Icons.home),
        actions: <Widget>[
          IconButton(
              onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?
                  spanishGreeting:englishGreeting;
                });
              },
              icon: Icon(Icons.refresh)),
          IconButton(
              onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?
                  JapanGreeting:JapanGreeting;
                });
              },
              icon: Icon(Icons.account_box)),
          IconButton(
              onPressed: (){
                setState(() {
                  displayText = displayText == englishGreeting?
                  ThaiGreetig:ThaiGreetig;
                });
              },
              icon: Icon(Icons.adb_outlined)),
    ],
      ),

    body: Center(
    child: Text(
    displayText,
      style: TextStyle(fontSize: 24),
    ),
    ),
    ),
    );
    return Container();
  }
}
